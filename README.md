# README #

https://globalgamejam.org/2018/games/fgjx-tic-tac-xoz

My submission to Finnish Game Jam X, the 2018 Jan 26-28 game making event. 
I taught myself a new language, C#, and coded this ASCII-game while learning in some 5 hours.

### How do I get set up? ###

The game is coded in C# and depends on .net framework.
It was meant mostly as a proof of concept and is best viewed as code, 
but for the stubborn user, I suggest using the same same environment I used: VS Code with .net Core.

### Who do I talk to? ###

Mikael Nenonen, done 2018
