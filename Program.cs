﻿using System;

namespace helloworld
{
    class Program
    {
        public Program() { }
        static Tuple<int, int> increase(Tuple<int, int> coords) {
            int x=coords.Item1;
            int y=coords.Item2;
            if(x != 3) {
                x++;
                return Tuple.Create<int, int>(x, y);
            }
            else {
                if (y != 3) {
                    y++;
                    x=0;
                    return Tuple.Create<int, int>(x, y);
                }
            }
            return Tuple.Create<int, int>(-1,-1);
        }
        static char[,] game = new char[4,4];
        
        static char data(int x, int y) {
            Tuple<int, int> coords = Tuple.Create(x, y);
            increase(coords);
            return game[x, y];
        }
        static void writeGrid() {
            int x=0, y=0;
            Console.WriteLine("  A B C D");
            Console.WriteLine();
            for(int i=1; i<5; i++) {
                Console.WriteLine("{0} {1}|{2}|{3}|{4}", i, data(x,y), data(x+1,y), data(x+2,y), data(x+3,y));
                y++;
                if(i!=4) 
                    Console.WriteLine("  -+-+-+-");
            }
        }
        static char findWinner() {
            char[] players = {'X','O','Z'};
            foreach(char any in players) {
                for(int j=0; j<4; j++) { // horizontal wins
                    if(game[0, j] == any && game[0, j] == game[1, j] && game[0, j] == game[2, j])
                        return any;
                    if(game[1, j] == any && game[1, j] == game[2, j] && game[1, j] == game[3, j])
                        return any;
                }
                for(int i=0; i<4; i++) { // vertical wins
                    if(game[i, 0] == any && game[i, 0] == game[i, 1] && game[i, 0] == game[i, 2])
                        return any;
                    if(game[i, 1] == any && game[i, 1] == game[i, 2] && game[i, 1] == game[i, 3])
                        return any;
                }
                for(int j=0; j<2; j++) { // diagonal wins starting from upper half
                    for(int i=0; i<2; i++) { // diagonal wins starting from left quarter
                        if(game[i, j] == any && game[i, j] == game[i+1, j+1] && game[i, j] == game[i+2, j+2])
                            return any;
                    }
                    for(int i=3; i>1; i--) { // diagonal wins starting from right quarter
                        if(game[i, j] == any && game[i, j] == game[i-1, j+1] && game[i, j] == game[i-2, j+2])
                            return any;
                    }
                }
                for(int j=3; j>1; j--) { // diagonal wins starting from lower half
                    for(int i=0; i<2; i++) { // diagonal wins starting from left quarter
                        if(game[i, j] == any && game[i, j] == game[i+1, j-1] && game[i, j] == game[i+2, j-2])
                            return any;
                    }
                    for(int i=3; i>1; i--) { // diagonal wins starting from right quarter
                        if(game[i, j] == any && game[i, j] == game[i-1, j-1] && game[i, j] == game[i-2, j-2])
                            return any;
                    }
                }
            }
            return ' ';
        }
        static char GameLogic(int turn) {
            char player;
            if (turn % 3 == 1)
                player = 'X';
            else if (turn % 3 == 2)
                player = 'O';
            else // (turn % 3 == 0)
                player = 'Z';
            Console.WriteLine("It's turn {0}: {1}'s turn. Move (like \"X A3\" or \"O4b\")", turn, player);
            Console.Write("> ");
            string line = Console.ReadLine();
            line = line.ToUpper().Trim();
            char numPart = ' ', alphaPart = ' ';
            if (line.StartsWith(player.ToString())) {// There's a right player's sign starting the line
                char[] rest = line.Substring(1).TrimStart().ToCharArray();
                if (char.IsLetter(rest[0]))
                    alphaPart = rest[0];
                else
                    numPart = rest[0];
                if (char.IsLetter(rest[1]))
                    alphaPart = rest[1];
                else
                    numPart = rest[1];
                int x = (int)alphaPart%(int)'A', y = numPart-(int)'1';
                if(char.IsLetter(alphaPart) && char.IsDigit(numPart) && 0 <= x && x <= 3 && 0 <= y && y <= 3) {
                    if(game[x, y] == ' ')
                        game[x, y] = player;
                    else {
                        Console.Beep();
                        Console.WriteLine("Illegal move! Turn lost.");
                    }
                } else {
                    Console.Beep();
                    Console.WriteLine("Illegal move! Turn lost.");
                }
            } else {
                    Console.Beep();
                    Console.WriteLine("Illegal move! Turn lost.");
            }
            return findWinner();
        }
        static bool anyLegalMovesLeft() {
            for(int j=0; j<4; j++) {
                for(int i=0; i<4; i++) {
                    if(game[i, j] == ' ')
                        return true;
                }
            }
            return false;
        }
        static void Main(string[] args)
        {
            for(int j=0; j<4; j++) {
                for(int i=0; i<4; i++) {
                    game[i, j] = ' ';
                }
            }

            //Console.WriteLine("Hello World!");
            int turn = 1;
            char winner = ' ';
            writeGrid();
            while(winner == ' ') {
                winner = GameLogic(turn);
                writeGrid();
                turn += 1;
                if(!anyLegalMovesLeft())
                    break;
            }
            if(winner != ' ')
                Console.WriteLine("Game ends! {0} wins!", winner);
            else
                Console.WriteLine("Draw. Thanks for playing!");
        }
    }
}
